//
//  Phone.swift
//  G63L5
//
//  Created by Ivan Vasilevich on 6/16/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class Phone: NSObject {
	
	private static var phonesMade = 0
	
	let screenSize: Double
	let memorySize: Int
	let color  = "white"
	
	var freeMemory  = 0
	var photosCount: Int {
		return _photosCount
	}
	var phoneName: String?
	override var description: String {
		var result = ""
		result += "phoneName = \(phoneName ?? "Unknown")\n"
		result += "photosCount = \(_photosCount)\n"
		result += "freeMemory = \(freeMemory)GB\n"
		result += "total phones count = \(Phone.phonesMade)"
		return result
	}
	
	private var _photosCount = 0
	
	init(screen: Double, memory: Int) {
		Phone.phonesMade += 1
		screenSize = screen
		memorySize = memory
		freeMemory = memory - 1
		
	}
	
	/// - parameter llamaCount: The number of llamas in the managed herd.
	/**
	Lorem ipsum dolor sit amet.
	
	@param bar Consectetur adipisicing elit.
	
	@return Sed do eiusmod tempor.
	*/
	func makePhoto() {
		_photosCount += 1
	}
	
	

}
