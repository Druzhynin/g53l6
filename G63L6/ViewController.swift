//
//  ViewController.swift
//  G63L6
//
//  Created by Kirill Druzhynin on 17.06.2018.
//  Copyright © 2018 Kirill Druzhynin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var centerLabel: UILabel!
    var phone: Phone!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        phone = Phone(screen: 5.5, memory: 16)
        
        centerLabel.textColor = UIColor.white
        
    }
    
    @IBAction func buttonPressed() {
        
        let textToDisplay = "pew pew \(arc4random()%30)"
        centerLabel.textColor = UIColor.blue
        centerLabel.text = textToDisplay
        
        
        
    }
    
}

